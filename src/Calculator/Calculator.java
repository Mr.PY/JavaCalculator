package Calculator;

import java.awt.*;
import java.awt.event.*;
import static java.awt.Label.*;
import static java.awt.event.KeyEvent.*;

class MyTheme{
    public static final Color FRAMEBACKGROUND = new Color(54, 54, 54);
    public static final Color FRAMEFOREGROUND = Color.WHITE;
    public static final Color LABELBACKGROUND = Color.BLACK;
    public static final Color LABELFOREGROUND = Color.GREEN;
    public static final Color PERNDELBACKGROUND = new Color(165, 165, 165);
    public static final Color PERNDELFOREGROUND = Color.BLACK;
    public static final Color CLEARBACKGROUND = new Color(165, 165, 165);
    public static final Color CLEARFOREGROUND = new Color(203, 12, 20);
    public static final Color OPERATORBACKGROUND = new Color(252, 151, 34);
    public static final Color OPERATORFOREGROUND = Color.WHITE;


}

public class Calculator extends Frame implements KeyListener{
    //    Variables for inputLabel Assignments
    private double firstValue=0, secondValue =0, operationValue;
    private String myValue;
    boolean firstValueEntered=false;
    private String inputValue, operation, displayValue;
    //    Elements for the Frame
    Label inputLabel = new Label("", RIGHT);
    Label operationLabel = new Label("", RIGHT);
    Button buttonOne = new Button("1");
    Button buttonTwo = new Button("2");
    Button buttonThree = new Button("3");
    Button buttonFour = new Button("4");
    Button buttonFive = new Button("5");
    Button buttonSix = new Button("6");
    Button buttonSeven = new Button("7");
    Button buttonEight = new Button("8");
    Button buttonNine = new Button("9");
    Button buttonZero = new Button("0");
    Button buttonDot = new Button(".");
    Button buttonPlusMinus = new Button("+/-");
    Button buttonBack = new Button("Del");
    Button buttonEquals = new Button("=");
    Button buttonClear = new Button("AC");
    Button buttonPlus = new Button("+");
    Button buttonMinus = new Button("-");
    Button buttonMultiply = new Button("*");
    Button buttonDivide = new Button("/");
    Button buttonModulus = new Button("%");
    Panel topPanel = new Panel();
    Panel buttonsPanel = new Panel();

    private void setUI(){
        Button[] buttons = {buttonClear,buttonBack, buttonModulus, buttonDivide, buttonSeven,
                buttonEight,buttonNine,buttonMultiply, buttonFour, buttonFive, buttonSix,
                buttonMinus,buttonOne,buttonTwo,buttonThree,buttonPlus,buttonDot,buttonZero,
                buttonPlusMinus,buttonEquals};
        topPanel.setLayout(new BorderLayout());
        buttonsPanel.setLayout(new GridLayout(5, 4));
        topPanel.add(operationLabel, BorderLayout.NORTH);
        topPanel.add(inputLabel, BorderLayout.CENTER);
        for (Button button : buttons) buttonsPanel.add(button);
        add(topPanel, BorderLayout.NORTH);
        add(buttonsPanel, BorderLayout.CENTER);
    }
    private void setTheme(){
        setFont(new Font("SansSerif", Font.PLAIN, 24));
        inputLabel.setFont(new Font("SansSerif", Font.PLAIN, 70));
        operationLabel.setFont(new Font("SansSerif", Font.PLAIN, 40));
        setBackground(MyTheme.FRAMEBACKGROUND);
        setForeground(MyTheme.FRAMEFOREGROUND);
        inputLabel.setBackground(MyTheme.LABELBACKGROUND);
        inputLabel.setForeground(MyTheme.LABELFOREGROUND);
        operationLabel.setBackground(MyTheme.LABELBACKGROUND);
        operationLabel.setForeground(MyTheme.LABELFOREGROUND);
        buttonModulus.setBackground(MyTheme.PERNDELBACKGROUND);
        buttonBack.setBackground(MyTheme.PERNDELBACKGROUND);
        buttonModulus.setForeground(MyTheme.PERNDELFOREGROUND);
        buttonBack.setForeground(MyTheme.PERNDELFOREGROUND);
        buttonClear.setBackground(MyTheme.CLEARBACKGROUND);
        buttonClear.setForeground(MyTheme.CLEARFOREGROUND);
        buttonPlus.setBackground(MyTheme.OPERATORBACKGROUND);
        buttonMinus.setBackground(MyTheme.OPERATORBACKGROUND);
        buttonMultiply.setBackground(MyTheme.OPERATORBACKGROUND);
        buttonDivide.setBackground(MyTheme.OPERATORBACKGROUND);
        buttonEquals.setBackground(MyTheme.OPERATORBACKGROUND);
        buttonPlus.setForeground(MyTheme.OPERATORFOREGROUND);
        buttonMinus.setForeground(MyTheme.OPERATORFOREGROUND);
        buttonMultiply.setForeground(MyTheme.OPERATORFOREGROUND);
        buttonDivide.setForeground(MyTheme.OPERATORFOREGROUND);
        buttonEquals.setForeground(MyTheme.OPERATORFOREGROUND);
    }
    //    Constructor
    private Calculator(){
        setTitle("Calculator");
        setLocation(650, 250);
        setSize(400, 500);
        setLayout(new BorderLayout());
        setUI();
        setTheme();
        addKeyListener(this);
        setFocusable(true);
//        Event Handlers
//        Button Handlers -- Handlers to do Arithmetic operations
        buttonOne.addActionListener(e -> number1());
        buttonTwo.addActionListener(e -> number2());
        buttonThree.addActionListener(e -> number3());
        buttonFour.addActionListener(e -> number4());
        buttonFive.addActionListener(e -> number5());
        buttonSix.addActionListener(e -> number6());
        buttonSeven.addActionListener(e -> number7());
        buttonEight.addActionListener(e -> number8());
        buttonNine.addActionListener(e -> number9());
        buttonZero.addActionListener(e -> number0());
        buttonDot.addActionListener(e -> dot());
        buttonPlusMinus.addActionListener(e -> plusMinus());
        buttonPlus.addActionListener(e -> add());
        buttonMinus.addActionListener(e -> subtract());
        buttonMultiply.addActionListener(e -> multiply());
        buttonDivide.addActionListener(e -> divide());
        buttonModulus.addActionListener(e -> percentage());
        buttonEquals.addActionListener(e -> {solution();requestFocus();});
        buttonBack.addActionListener(e -> {delete();requestFocus();});
        buttonClear.addActionListener(e -> {clear();requestFocus();});

        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                System.exit(0);
            }
        });
        setVisible(true);
    }   // End of Constructor

    //    Events for Key presses
    @Override
    public void keyTyped(KeyEvent e){ }
    @Override
    public void keyPressed(KeyEvent e){
        int keyPressed = e.getKeyCode();
        if(keyPressed == VK_NUMPAD1 || keyPressed == VK_1){
            number1();
        } else if(keyPressed == VK_NUMPAD2 || keyPressed == VK_2){
            number2();
        }else if(keyPressed == VK_NUMPAD3 || keyPressed == VK_3){
            number3();
        }else if(keyPressed == VK_NUMPAD4 || keyPressed == VK_4){
            number4();
        }else if(keyPressed == VK_NUMPAD5 || keyPressed == VK_5){
            number5();
        }else if(keyPressed == VK_NUMPAD6 || keyPressed == VK_6){
            number6();
        }else if(keyPressed == VK_NUMPAD7 || keyPressed == VK_7){
            number7();
        }else if(keyPressed == VK_NUMPAD8 || keyPressed == VK_8){
            number8();
        }else if(keyPressed == VK_NUMPAD9 || keyPressed == VK_9){
            number9();
        }else if(keyPressed == VK_NUMPAD0 || keyPressed == VK_0){
            number0();
        }else if(keyPressed == VK_PERIOD || keyPressed == VK_DECIMAL){
            dot();
        }else if(keyPressed == VK_ADD || keyPressed == VK_PLUS){
            add();
        }else if(keyPressed == VK_SUBTRACT || keyPressed == VK_MINUS){
            subtract();
        }else if(keyPressed == VK_MULTIPLY || keyPressed == VK_ASTERISK){
            multiply();
        }else if(keyPressed == VK_SLASH || keyPressed == VK_DIVIDE){
            divide();
        }else if(keyPressed == VK_ENTER || keyPressed == VK_EQUALS){
            solution();
        }else if(keyPressed == VK_BACK_SPACE){
            delete();
        }else if(keyPressed == VK_ESCAPE){
            clear();
        }
    }
    @Override
    public void keyReleased(KeyEvent e){ }
    //    Methods to perform actions and assign values
    private void displayValue(String s){
        if(inputLabel.getText() == null){myValue = s;}
        else{myValue= inputLabel.getText().concat(s);}
        if (!firstValueEntered){
            inputLabel.setText(myValue);
            firstValue = Double.parseDouble(inputLabel.getText());
            displayValue = String.valueOf(firstValue);
            operationLabel.setText(displayValue);
        }else{
            inputLabel.setText(myValue);
            secondValue = Double.parseDouble(inputLabel.getText());
//            displayValue = displayValue +" "+ secondValue;
//            operationLabel.setText(displayValue);
            System.out.println("First inputValue is : "+ firstValue);
            System.out.println("Second inputValue is : "+ secondValue);
        }
    }
    private void operationDisplay(){
        System.out.println("operator is : "+operation.charAt(0));
        System.out.println("Display Last character is : "+displayValue.charAt(displayValue.length()-1));
        char x = displayValue.charAt(displayValue.length() - 1);
        if (!(x == '+' || x == '-' || x == '*' || x == '/')) {
                inputLabel.setText("");
                displayValue = displayValue + " " + operation;
            operationLabel.setText(displayValue);
            firstValueEntered = true;
        }else{
            displayValue = displayValue.replace(displayValue.substring(displayValue.length() - 1), operation);
            operationLabel.setText(displayValue);
            System.out.println("New Operator is : " + operation);

        }
    }
    private void result(){
        switch (operation){
            case "+":
                operationValue = firstValue + secondValue;
                break;
            case "-":
                operationValue = firstValue - secondValue;
                break;
            case "*":
                operationValue = firstValue * secondValue;
                break;
            case "/":
                operationValue = firstValue / secondValue;
                break;
            case "%":
                operationValue = firstValue % secondValue;
                break;
        }
    }
    private void number1(){
        inputValue = Integer.toString(1);
        displayValue(inputValue);
    }
    private void number2(){
        inputValue = Integer.toString(2);
        displayValue(inputValue);
    }
    private void number3(){
        inputValue = Integer.toString(3);
        displayValue(inputValue);
    }
    private void number4(){
        inputValue = Integer.toString(4);
        displayValue(inputValue);
    }
    private void number5(){
        inputValue = Integer.toString(5);
        displayValue(inputValue);
    }
    private void number6(){
        inputValue = Integer.toString(6);
        displayValue(inputValue);
    }
    private void number7(){
        inputValue = Integer.toString(7);
        displayValue(inputValue);
    }
    private void number8(){
        inputValue = Integer.toString(8);
        displayValue(inputValue);
    }
    private void number9(){
        inputValue = Integer.toString(9);
        displayValue(inputValue);
    }
    private void number0(){
        inputValue = Integer.toString(0);
        displayValue(inputValue);
    }
    private void dot(){
        String decimal = ".";
        String decimalValue = inputLabel.getText();
        if(!decimalValue.contains(decimal)){
            inputLabel.setText(inputLabel.getText() + ".");
        }
    }
    private void plusMinus(){
        String flipValue = String.valueOf(Double.parseDouble(inputLabel.getText())* -1);
        inputLabel.setText(flipValue);
        if(secondValue ==0) {
            firstValue = Double.parseDouble(inputLabel.getText());
            displayValue = String.valueOf(firstValue);
        } else{
            secondValue =Double.parseDouble(inputLabel.getText());
      }
        operationLabel.setText(displayValue);
    }
    private void add(){
        operation = "+";
        operationDisplay();
    }
    private void subtract(){
        operation = "-";
        operationDisplay();
    }
    private void multiply(){
        operation = "*";
        operationDisplay();
    }
    private void divide(){
        operation = "/";
        operationDisplay();
    }
    private void percentage(){
        operation = "%";
        operationDisplay();
    }
    private void solution(){
        result();
        inputLabel.setText(String.valueOf(operationValue));
        firstValue = operationValue;
        if(secondValue ==0) {
            displayValue = operationLabel.getText();
        } else{
            displayValue = operationLabel.getText() +" "+ secondValue;
        }
        secondValue = 0;
        operationLabel.setText(displayValue);
    }
    private void delete(){
        String deleteValue = inputLabel.getText();
        try{
            if (!deleteValue.equals("")){
                if(secondValue==0){
                    inputLabel.setText(deleteValue.substring(0, deleteValue.length()-1));
                    firstValue = Double.parseDouble(inputLabel.getText());
                    displayValue = String.valueOf(firstValue);
                    operationLabel.setText(String.valueOf(firstValue));
                }else{
                    inputLabel.setText(deleteValue.substring(0, deleteValue.length()-1));
                    secondValue = Double.parseDouble(inputLabel.getText());
                }
            }else{
                inputLabel.setText("");
                operationLabel.setText("");
            }
        }catch(NumberFormatException e){
            System.out.println("Input Label is Empty");
        }
    }
    private void clear(){
        inputLabel.setText("");
        operationLabel.setText("");
        firstValue = 0;
        secondValue = 0;
        firstValueEntered = false;
    }
    public static void main(String[] args){
        new Calculator();
    }
}